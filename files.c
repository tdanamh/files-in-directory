#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <time.h>
#include <pwd.h>
#include<sys/types.h>

char *getUser(uid_t uid)
{
    struct passwd *pws;
    pws = getpwuid(uid);
        return pws->pw_name;
}

int signfile(char *director)
{
	int current = time(0);
	int userId;
	DIR *dir = opendir(director);
	
	if(dir == NULL){
		printf("dcaca");
		return -1;
	}

	chdir(director);
	struct dirent *entry; 
	struct stat entrystat;
	struct tm *timer;
	int year;
	char current_year[5];
	time_t now = time(0);
	int nr = 0;
	strftime(current_year,1000,"%Y",localtime(&now));
	
	entry = readdir(dir); //intrare director
	if(entry == NULL) return -1;

	while(entry != NULL)
	{
		if ((strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0))
		{	
			FILE *f = fopen(entry->d_name,"a");
			if(f == NULL){
				return -1;
			}
			if (entry->d_type == DT_REG) {
				if (stat(entry->d_name, &entrystat) == 0) { //success
					nr ++;
					//last modified date year
					timer = gmtime(&entrystat.st_mtime);
					year = timer->tm_year + 1900;
					if(year == 2020) {
						userId = entrystat.st_uid;
						char name[200];
						strcpy(name, getUser(userId));
						if(fputs(name, f) == EOF){
							return -1; 
						}
					}
				}
				else {
					return -1;
				}
			}
		}
		entry = readdir(dir);
	}
	closedir(dir);
	return nr;
}

int main()
{
	printf("Nr:%d ", signfile("lista3"));
	return 0;
}
